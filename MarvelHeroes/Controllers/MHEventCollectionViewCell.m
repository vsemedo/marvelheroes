//
//  MHEventCollectionViewCell.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 22/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHEventCollectionViewCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@implementation MHEventCollectionViewCell

- (void)setEvent:(MHEvent *)event
{
    _event = event;
    self.uiLblTitle.text = event.title;
    self.uiLblDescription.text = event.eventDescription;
    [self setEventImage];
}

#pragma mark - Private Methods

- (void)startActivityIndicator
{
    self.uiActLoading.hidden = NO;
    self.uiImgEvent.hidden = YES;
    [self.uiActLoading startAnimating];
}

- (void)stopActivityIndicator
{
    self.uiActLoading.hidden = YES;
    self.uiImgEvent.hidden = NO;
    [self.uiActLoading stopAnimating];
}

- (void)setEventImage
{
    NSURLRequest *request = nil;
    
    self.uiImgEvent.contentMode = UIViewContentModeScaleAspectFit;
    [self startActivityIndicator];
    if (_event.image) {
        self.uiImgEvent.image = [UIImage imageWithData:_event.image];
        [self stopActivityIndicator];
    }else if (_event.imageUrl.length > 0) {
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:_event.imageUrl]];
        [self.uiImgEvent setImageWithURLRequest:request
                              placeholderImage:nil
                                       success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                           _event.image = UIImageJPEGRepresentation(image, 1.0);
                                           [self.uiImgEvent setImage:image];
                                           [self stopActivityIndicator];
                                       }
                                       failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                           _event.image = nil;
                                           [self.uiImgEvent setImage:nil];
                                           [self stopActivityIndicator];
                                       }];
    }else {
        [self.uiImgEvent setImage:nil];
        [self stopActivityIndicator];
    }
}

@end
