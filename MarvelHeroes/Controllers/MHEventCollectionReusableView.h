//
//  MHEventCollectionReusableView.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 22/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHHero.h"

@interface MHEventCollectionReusableView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *uiLblHeroName;

@property (weak, nonatomic) IBOutlet UILabel *uiLblHeroDescription;

@property (weak, nonatomic) IBOutlet UIButton *uiButtonFavorite;

@property (nonatomic, strong)MHHero* hero;

- (void)setHero:(MHHero *)hero;

@end
