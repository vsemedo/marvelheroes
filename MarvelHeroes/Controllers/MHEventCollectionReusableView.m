//
//  MHEventCollectionReusableView.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 22/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHEventCollectionReusableView.h"
#import "MHDataManager.h"

@implementation MHEventCollectionReusableView


- (void)setHero:(MHHero *)hero
{
    _hero = hero;
    self.uiLblHeroName.text = self.hero.name;
    self.uiLblHeroDescription.text = self.hero.heroDescription;
    [self setFavoriteImg];
}

- (IBAction)setFavorite:(id)sender {
    
    self.hero.isFavorite = !self.hero.isFavorite;
    [self setFavoriteImg];
    
    if (self.hero.isFavorite) {
        [MHDataManager saveHero:self.hero];
    }else {
        [MHDataManager deleteHero:self.hero.heroID];
    }
}

- (void)setFavoriteImg
{
    if(self.hero.isFavorite) {
        [self.uiButtonFavorite setImage:[UIImage imageNamed:@"favorite"] forState:UIControlStateNormal];
    }else {
        [self.uiButtonFavorite setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
    }
}
@end
