//
//  MHHeroTableViewCell.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHHero.h"
#import "MHHeroesTableViewController.h"

@interface MHHeroTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *uiImgHero;

@property (weak, nonatomic) IBOutlet UILabel *uiLblName;

@property (weak, nonatomic) IBOutlet UILabel *uiLblDescription;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *uiActLoading;

@property (weak, nonatomic) IBOutlet UIButton *uiBtnFavorite;

@property (nonatomic, strong)MHHero *hero;

@property (nonatomic, strong)MHHeroesTableViewController* parentVc;

@end
