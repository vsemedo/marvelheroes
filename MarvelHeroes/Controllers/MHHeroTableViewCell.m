//
//  MHHeroTableViewCell.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHHeroTableViewCell.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "MHDataManager.h"

@implementation MHHeroTableViewCell

- (void)setHero:(MHHero *)hero
{
    _hero = hero;
    [self setHeroImage];
    [self setFavorite];
    self.uiLblName.text = hero.name;
    self.uiLblDescription.text = hero.heroDescription;
}

#pragma mark - UIButton Actions


- (IBAction)favoriteButtonClick:(id)sender
{
    [self.parentVc setHero:self.hero isFavorite:!self.hero.isFavorite];
}

#pragma mark - Private Methods

- (void)startActivityIndicator
{
    self.uiActLoading.hidden = NO;
    self.uiImgHero.hidden = YES;
    [self.uiActLoading startAnimating];
}

- (void)stopActivityIndicator
{
    self.uiActLoading.hidden = YES;
    self.uiImgHero.hidden = NO;
    [self.uiActLoading stopAnimating];
}

- (void)setHeroImage
{
    NSURLRequest *request = nil;
    
    self.uiImgHero.contentMode = UIViewContentModeScaleAspectFit;
    [self startActivityIndicator];
    if (_hero.image) {
        self.uiImgHero.image = [UIImage imageWithData:_hero.image];
        [self stopActivityIndicator];
    }else if (_hero.imageUrl.length > 0) {
        request = [NSURLRequest requestWithURL:[NSURL URLWithString:_hero.imageUrl]];
        [self.uiImgHero setImageWithURLRequest:request
                              placeholderImage:nil
                                       success:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, UIImage * _Nonnull image) {
                                           _hero.image = UIImageJPEGRepresentation(image, 1.0);
                                           [self.uiImgHero setImage:image];
                                           [self stopActivityIndicator];
                                       }
                                       failure:^(NSURLRequest * _Nonnull request, NSHTTPURLResponse * _Nullable response, NSError * _Nonnull error) {
                                           _hero.image = nil;
                                           [self.uiImgHero setImage:nil];
                                           [self stopActivityIndicator];
                                       }];
    }else {
        [self.uiImgHero setImage:nil];
        [self stopActivityIndicator];
    }
}

- (void)setFavorite
{
    if(self.hero.isFavorite) {
        [self.uiBtnFavorite setImage:[UIImage imageNamed:@"favorite"] forState:UIControlStateNormal];
    }else {
        [self.uiBtnFavorite setImage:[UIImage imageNamed:@"star"] forState:UIControlStateNormal];
    }
}

@end
