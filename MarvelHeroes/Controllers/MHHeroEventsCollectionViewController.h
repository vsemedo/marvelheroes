//
//  MHHeroEventsCollectionViewController.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 22/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHHero.h"

@interface MHHeroEventsCollectionViewController : UICollectionViewController

@property(nonatomic, strong)MHHero* hero;

@end
