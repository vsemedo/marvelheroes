//
//  MHEventCollectionViewCell.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 22/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHEvent.h"

@interface MHEventCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *uiImgEvent;

@property (weak, nonatomic) IBOutlet UILabel *uiLblTitle;

@property (weak, nonatomic) IBOutlet UILabel *uiLblDescription;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *uiActLoading;

@property (nonatomic, strong)MHEvent* event;

@end
