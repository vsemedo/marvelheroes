//
//  MHHeroEventsCollectionViewController.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 22/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHHeroEventsCollectionViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "MHMarvelApiManager.h"
#import "MHEventCollectionViewCell.h"
#import "MHEvent.h"
#import "MHEventCollectionReusableView.h"

@interface MHHeroEventsCollectionViewController ()

@property(nonatomic, strong)NSMutableArray* events;

@end

@implementation MHHeroEventsCollectionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.events = [NSMutableArray new];
    [self loadEvents];
}

- (void)loadEvents
{
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    [SVProgressHUD show];
    [MHMarvelApiManager fetchCharacterEvents:self.hero.heroID
                              withParameters:parameters
                          andComplitionBlock:^(NSArray *result, NSError *error) {
                              if (error) {
                                  [SVProgressHUD dismissWithCompletion:^{
                                      [self showAlert];
                                  }];
                              }else {
                                  [self.events addObjectsFromArray:result];
                                  [self.collectionView reloadData];
                                  [SVProgressHUD dismiss];
                              }
                          }];
}

- (void)showAlert
{
    UIAlertController *alertController = nil;
    UIAlertAction* ok = nil;
    
    alertController = [UIAlertController alertControllerWithTitle:@"Erro"
                                                          message:@"Ocorreu um erro ao carregar os dados. Verifique a sua conexão"
                                                   preferredStyle:UIAlertControllerStyleAlert];
    ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.events.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MHEvent *event = nil;
    MHEventCollectionViewCell *cell = nil;
    
    event = self.events[indexPath.row];
    cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MHEventCollectionViewCell" forIndexPath:indexPath];
    cell.event = event;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        MHEventCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MHEventCollectionReusableView" forIndexPath:indexPath];
        headerView.hero = self.hero;
        
        reusableview = headerView;
    }
    
    return reusableview;
}


@end
