//
//  MHHeroesTableViewController.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHHeroesTableViewController.h"
#import "MHMarvelApiManager.h"
#import "MHHeroTableViewCell.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "MHHeroEventsCollectionViewController.h"
#import "MHDataManager.h"

@interface MHHeroesTableViewController ()<UISearchBarDelegate>

@property(nonatomic, strong)NSMutableArray* heroes;

@property(nonatomic, strong)NSMutableArray* filteredHeroes;

@property(nonatomic, strong)NSMutableArray* sortedArray;

@property (nonatomic, strong)NSArray *favorites;

@property(nonatomic, strong)NSString* filter;

@property(nonatomic, assign)BOOL allLoaded;

@property(nonatomic, assign)NSInteger selectedRow;

@property (weak, nonatomic) IBOutlet UISwitch *uiSwtSort;

@property (nonatomic, assign)NSInteger currentOffset;

@end

@implementation MHHeroesTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.heroes = [NSMutableArray new];
    self.currentOffset = 0;
    self.allLoaded = NO;
    [self loadHeroes:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.favorites = [MHDataManager fetchHeroes];
    [self udpateCurrentArray];
}

- (void)loadHeroes:(BOOL)isFirstLoad
{
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    NSNumber* offset = nil;
    
    if (self.filter.length > 0 || !self.allLoaded) {
     
        if (self.filter.length > 0) {
            parameters[@"nameStartsWith"] = self.filter;
            offset = [NSNumber numberWithInteger:0];
        }else {
            offset = [NSNumber numberWithInteger:self.currentOffset];
        }
        
        parameters[@"offset"] = offset;
        parameters[@"limit"] = [NSNumber numberWithInteger:100];
        
        [SVProgressHUD show];
        
        if(isFirstLoad) {
            isFirstLoad = NO;
            self.favorites = [MHDataManager fetchHeroes];
            self.sortedArray = self.favorites.mutableCopy;
        }
        
        [MHMarvelApiManager fetchCharactersWithParameters:parameters
                                       andComplitionBlock:^(NSArray *result, NSError *error) {
                                           
                                           if (error) {
                                               [SVProgressHUD dismissWithCompletion:^{
                                                   [self showAlert];
                                                   [self.tableView reloadData];
                                                   
                                               }];
                                           }else {
                                               if(self.filter.length > 0) {
                                                   self.filteredHeroes = result.mutableCopy;
                                               }else {
                                                   [self.heroes addObjectsFromArray:result];
                                                   
                                                   self.currentOffset += result.count;
                                                   if (result.count < 100) {
                                                       self.allLoaded = YES;
                                                   }
                                               }
                                               [self udpateCurrentArray];
                                               [SVProgressHUD dismiss];
                                           }
                                       }];
    }
}

- (void)showAlert
{
    UIAlertController *alertController = nil;
    UIAlertAction* ok = nil;
    
    alertController = [UIAlertController alertControllerWithTitle:@"Erro"
                                                          message:@"Ocorreu um erro ao carregar os dados. Verifique a sua conexão"
                                                   preferredStyle:UIAlertControllerStyleAlert];
    ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)updatefilteredWithFavorites
{
    NSArray *uniqueHeroesID = nil;
    NSPredicate *predicate = nil;
    
    uniqueHeroesID = [self.favorites valueForKeyPath:@"@distinctUnionOfObjects.heroID"];
    predicate = [NSPredicate predicateWithFormat:@"heroID IN %@", uniqueHeroesID];
    
    for (MHHero *hero in [self.sortedArray filteredArrayUsingPredicate:predicate]) {
        hero.isFavorite = YES;
    }
}

- (void)updateWithFavorites
{
    NSArray *uniqueHeroesID = nil;
    NSPredicate *predicate = nil;
    NSArray *removeFav = nil;
    
    uniqueHeroesID = [self.favorites valueForKeyPath:@"@distinctUnionOfObjects.heroID"];
    predicate = [NSPredicate predicateWithFormat:@"NOT (heroID IN %@)", uniqueHeroesID];
    removeFav = [self.sortedArray filteredArrayUsingPredicate:predicate];
    
    self.sortedArray = self.favorites.mutableCopy;
    [self.sortedArray addObjectsFromArray:removeFav];
    
}

- (void)udpateCurrentArray
{
    if (self.filter.length > 0) {
        self.sortedArray = self.filteredHeroes;
        [self updatefilteredWithFavorites];
    }else {
        self.sortedArray = self.heroes;
        [self updateWithFavorites];
    }
    
    if (self.uiSwtSort.isOn) {
        self.sortedArray = [self.sortedArray sortedArrayUsingComparator:^(MHHero *firstObject, MHHero *secondObject) {
            if (firstObject.isFavorite == secondObject.isFavorite) {
                return [firstObject.name compare:secondObject.name];
            }else if (firstObject.isFavorite) {
                return NSOrderedAscending;
            }else {
                return NSOrderedDescending;
            }
        }].mutableCopy;
    }else {
        self.sortedArray = [self.sortedArray sortedArrayUsingComparator:^(MHHero *firstObject, MHHero *secondObject) {
            if (firstObject.isFavorite && !secondObject.isFavorite) {
                return NSOrderedAscending;
            }else if (!firstObject.isFavorite && secondObject.isFavorite) {
                return NSOrderedDescending;
            }else {
                return NSOrderedSame;
            }
        }].mutableCopy;
    }
    
    [self.tableView reloadData];
}

- (void)setHero:(MHHero*)hero isFavorite:(BOOL)isFavorite
{
    hero.isFavorite = isFavorite;
    
    if (isFavorite) {
        [MHDataManager saveHero:hero];
    }else {
        [MHDataManager deleteHero:hero.heroID];
    }
    
    self.favorites = [MHDataManager fetchHeroes];
    [self udpateCurrentArray];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showHeroEvents"])
    {
        MHHeroEventsCollectionViewController *eventsVC = [segue destinationViewController];
        
        if (self.filter.length > 0) {
            eventsVC.hero = self.sortedArray[_selectedRow];
        }else {
            eventsVC.hero = self.sortedArray[_selectedRow];
        }
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.sortedArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MHHeroTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MHHeroTableViewCell"];
    
    if (cell == nil) {
        cell = [[MHHeroTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                          reuseIdentifier:@"MHHeroTableViewCell"];
    }
    
    cell.parentVc = self;
    cell.hero = self.sortedArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    self.selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"showHeroEvents" sender:self];
}

#pragma mark - UIScrollView

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat actualPosition = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height - (self.tableView.frame.size.height);
    if (actualPosition >= contentHeight) {
        [self loadHeroes:NO];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    self.filter = searchText;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.filter = nil;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    self.favorites = [MHDataManager fetchHeroes];
    [self udpateCurrentArray];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [self loadHeroes:NO];
}

- (IBAction)sortByName:(id)sender
{
    [self udpateCurrentArray];
}

@end
