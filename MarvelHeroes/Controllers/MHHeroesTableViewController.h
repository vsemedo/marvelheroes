//
//  MHHeroesTableViewController.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHHero.h"

@interface MHHeroesTableViewController : UITableViewController

- (void)setHero:(MHHero*)hero isFavorite:(BOOL)isFavorite;


@end
