//
//  MHEvent.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Event+CoreDataClass.h"

@interface MHEvent : NSObject

@property(nonatomic, strong)NSString* eventID;

@property(nonatomic, strong)NSString* title;

@property(nonatomic, strong)NSString* eventDescription;

@property(nonatomic, strong)NSString* imageUrl;

@property(nonatomic, strong)NSData* image;


- (instancetype)initWithJson:(NSDictionary*)json;

- (instancetype)initWithObj:(Event*)eventObj;

@end
