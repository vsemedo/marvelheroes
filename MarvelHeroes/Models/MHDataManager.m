//
//  MHDataManager.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHDataManager.h"
#import <CoreData/NSManagedObjectContext.h>
#import "AppDelegate.h"

@implementation MHDataManager

+ (NSManagedObjectContext*)getContext
{
    NSManagedObjectContext *context = ((AppDelegate*)[[UIApplication sharedApplication] delegate]).persistentContainer.viewContext;

    return context;
}


+ (Hero*)saveHero:(MHHero*)hero
{
    NSError *error = nil;
    Hero *newHero = nil;
    
    newHero = [MHDataManager fetchHero:hero.heroID];
    
    if (!newHero) {
        newHero = [NSEntityDescription insertNewObjectForEntityForName:@"Hero"
                                                inManagedObjectContext:[MHDataManager getContext]];
    }
    
    [newHero setValue:hero.imageUrl forKey:@"imageUrl"];
    [newHero setValue:hero.heroDescription forKey:@"heroDescription"];
    [newHero setValue:hero.name forKey:@"name"];
    [newHero setValue:hero.heroID forKey:@"heroID"];

    if (hero.image) {
        [newHero setValue:hero.image forKey:@"image"];
    }
    
    @try {
        [[MHDataManager getContext] save:&error];
        
        if (error) {
            return nil;
        }
    } @catch (NSException *exception) {
        return nil;
    }
    
    return newHero;
}

+ (BOOL)deleteHero:(NSString*)heroID
{
    Hero *hero = nil;
    BOOL result = NO;
    
    hero = [MHDataManager fetchHero:heroID];

    if (hero) {
        [[MHDataManager getContext] deleteObject:hero];
        result = YES;
    }
    
    return result;
}

+ (Hero*)fetchHero:(NSString*)heroID
{
    NSManagedObjectContext* context = [MHDataManager getContext];
    NSFetchRequest *request = nil;
    NSError *error = nil;
    NSArray *heroesData = nil;
    Hero *result = nil;
    
    request = [[NSFetchRequest alloc] initWithEntityName:@"Hero"];
    request.predicate = [NSPredicate predicateWithFormat:@"heroID == %@", heroID];
    
    [context executeFetchRequest:request error:&error];

    @try {
        heroesData = [context executeFetchRequest:request error:&error];
        
        if (error) {
            return nil;
        }else if (heroesData.count > 0){
            result = heroesData[0];
        }
    } @catch (NSException *exception) {
        return nil;
    }
    
    return result;
}

+ (NSArray*)fetchHeroes
{
    NSManagedObjectContext* context = [MHDataManager getContext];
    NSMutableArray* heroes = [NSMutableArray new];
    NSFetchRequest *request = nil;
    NSError *error = nil;
    NSArray *heroesData = nil;
    
    request = [[NSFetchRequest alloc] initWithEntityName:@"Hero"];
    
    @try {
        heroesData = [context executeFetchRequest:request error:&error];
        
        if (error) {
            return [NSMutableArray new];
        }else {
            for (Hero *heroData in heroesData) {
                [heroes addObject:[[MHHero alloc] initWithObj:heroData]];
            }
        }
    } @catch (NSException *exception) {
        return [NSMutableArray new];
    }
    
    return heroes;
}

@end
