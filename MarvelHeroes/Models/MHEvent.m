//
//  MHEvent.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHEvent.h"

@implementation MHEvent

- (instancetype)initWithJson:(NSDictionary*)json
{
    NSString *path = nil;
    NSString *extension = nil;
    
    self = [super init];
    
    if(self) {
        self.title = json[@"title"];
        self.eventID = [json[@"id"] stringValue];
        
        if (json[@"description"]) {
            self.eventDescription = json[@"description"];
        }
        
        if (json[@"thumbnail"] && [json[@"thumbnail"] count] > 0) {
            path = json[@"thumbnail"][@"path"];
            extension = json[@"thumbnail"][@"extension"];
            
            if (path.length > 0 && extension.length > 0) {
                self.imageUrl = [NSString stringWithFormat:@"%@.%@", path, extension];
            }
        }
    }
    
    return self;
}

- (instancetype)initWithObj:(Event*)eventObj
{
    self = [super init];
    
    if(self) {
        self.title = eventObj.title;
        self.eventID = eventObj.eventID;
        self.eventDescription = eventObj.eventDescription;
        self.imageUrl = eventObj.imageUrl;
        self.image = eventObj.image;
    }
    
    return self;
}

@end
