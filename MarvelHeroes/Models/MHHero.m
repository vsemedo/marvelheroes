//
//  MHHeroe.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHHero.h"
#import "Hero+CoreDataClass.h"

@implementation MHHero

- (instancetype)initWithJson:(NSDictionary*)json
{
    NSString *path = nil;
    NSString *extension = nil;
    
    self = [super init];
    
    if(self) {
        self.name = json[@"name"];
        self.heroID = [json[@"id"] stringValue];
        
        if (json[@"description"]) {
            self.heroDescription = json[@"description"];
        }
        
        if (json[@"thumbnail"] && [json[@"thumbnail"] count] > 0) {
            path = json[@"thumbnail"][@"path"];
            extension = json[@"thumbnail"][@"extension"];
            
            if (path.length > 0 && extension.length > 0) {
                self.imageUrl = [NSString stringWithFormat:@"%@.%@", path, extension];
            }
        }
        self.isFavorite = NO;
    }
    
    return self;
}

- (instancetype)initWithObj:(Hero*)heroObj
{
    self = [super init];
    
    if(self) {
        self.name = heroObj.name;
        self.heroID = heroObj.heroID;
        self.heroDescription = heroObj.heroDescription;
        self.imageUrl = heroObj.imageUrl;
        self.image = heroObj.image;
        self.isFavorite = YES;
    }
    
    return self;
}
@end
