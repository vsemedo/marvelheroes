//
//  MHMarvelApiManager.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface MHMarvelApiManager : AFHTTPSessionManager

+ (void)fetchCharactersWithParameters:(NSMutableDictionary*)parameters
                   andComplitionBlock:(void (^)(NSArray* result, NSError*error))block;

+ (void)fetchCharacterEvents:(NSString*)characterID
              withParameters:(NSDictionary*)parameters
          andComplitionBlock:(void (^)(NSArray* result, NSError*error))block;

@end
