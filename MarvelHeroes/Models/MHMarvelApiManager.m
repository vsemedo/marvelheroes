//
//  MHMarvelApiManager.m
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import "MHMarvelApiManager.h"
#import "NSString+MD5.h"
#import "MHHero.h"
#import "MHEvent.h"

static NSString* const API_PUBLIC_KEY = @"5ad16feab35d3d17b363c0610cbebbf1";

static NSString* const API_PRIVATE_KEY = @"b427e67c5de72562fd483c7b00dce2487b50ff3c";

static NSString* const API_BASE_URL = @"http://gateway.marvel.com";


@interface MHMarvelApiManager ()

@property(nonatomic, strong)NSString *timeStampString;

@property(nonatomic, strong)NSString *hashString;

@end

@implementation MHMarvelApiManager

#pragma mark - Constructors

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.timeStampString = [MHMarvelApiManager getTimesTampStr];
        self.hashString = [MHMarvelApiManager getHashStr:self.timeStampString];
    }
    
    return self;
}


#pragma mark - Public Methods

+ (instancetype)sharedMarvelApi
{
    static MHMarvelApiManager *instance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        instance = [[MHMarvelApiManager alloc] initWithBaseURL:[NSURL URLWithString:API_BASE_URL]];
    });
    
    return instance;
}


+ (void)fetchCharactersWithParameters:(NSDictionary*)parameters
               andComplitionBlock:(void (^)(NSArray* result, NSError*error))block
{
    NSDictionary *apiParameters = nil;
    
    apiParameters = [MHMarvelApiManager authenticationParameters];
    if (parameters.count > 0) {
        [apiParameters setValuesForKeysWithDictionary:parameters];
    }
    
    [[MHMarvelApiManager sharedMarvelApi] GET:@"/v1/public/characters"
                                   parameters:apiParameters
                                     progress:nil
                                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable json) {
                                          NSArray *heroes = nil;
                                          
                                          if (block) {
                                              heroes = [MHMarvelApiManager loadHeroesArrayFromJson:json];
                                              block(heroes, nil);
                                          }
                                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                          if (block) {
                                              block(nil, error);
                                          }
                                      }];
    
}

+ (void)fetchCharacterEvents:(NSString*)characterID
              withParameters:(NSDictionary*)parameters
          andComplitionBlock:(void (^)(NSArray* result, NSError*error))block
{
  
    NSDictionary *apiParameters = nil;
    NSString *urlString = nil;
    
    urlString = [MHMarvelApiManager getCharacterEventsURLString:characterID];
    
    apiParameters = [MHMarvelApiManager authenticationParameters];
    if (parameters.count > 0) {
        [apiParameters setValuesForKeysWithDictionary:parameters];
    }
    
    [[MHMarvelApiManager sharedMarvelApi] GET:urlString
                                   parameters:apiParameters
                                     progress:nil
                                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable json) {
                                          NSArray *events = nil;

                                          if (block) {
                                              events = [MHMarvelApiManager loadEventsArrayFromJson:json];
                                              block(events, nil);
                                          }
                                      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                                          if (block) {
                                              block(nil, error);
                                          }
                                      }];
}

#pragma mark - Private Methods

+ (NSString*)getCharacterEventsURLString:(NSString*)characterID
{
    NSString *urlString = @"/v1/public/characters/%@/events";
    
    urlString = [NSString stringWithFormat:urlString, characterID];
    
    return urlString;
}

+ (NSMutableDictionary*)authenticationParameters
{
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    
    parameters[@"ts"] = [MHMarvelApiManager sharedMarvelApi].timeStampString;
    parameters[@"apikey"] = API_PUBLIC_KEY;
    parameters[@"hash"] = [MHMarvelApiManager sharedMarvelApi].hashString;
    
    return parameters;
}

+ (NSMutableArray*)loadHeroesArrayFromJson:(NSDictionary*)json
{
    NSMutableArray* heroes = [NSMutableArray new];
    MHHero *heroe = nil;
    
    for (NSDictionary* heroeJson in json[@"data"][@"results"]) {
        heroe = [[MHHero alloc] initWithJson:heroeJson];
        [heroes addObject:heroe];
    }
    
    return heroes;
}

+ (NSMutableArray*)loadEventsArrayFromJson:(NSDictionary*)json
{
    NSMutableArray* events = [NSMutableArray new];
    MHEvent *event = nil;
    
    for (NSDictionary* eventJson in json[@"data"][@"results"]) {
        event = [[MHEvent alloc] initWithJson:eventJson];
        [events addObject:event];
    }
    
    return events;
}

+ (NSString*)getTimesTampStr
{
    NSString* timesTamp = nil;
    
    timesTamp = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
    
    return timesTamp;
}

+ (NSString*)getHashStr:(NSString*)timesTamp
{
    NSString* hash = nil;
    
    hash = [NSString stringWithFormat:@"%@%@%@", timesTamp, API_PRIVATE_KEY, API_PUBLIC_KEY];
    
    return [hash getMD5Str];
}



@end
