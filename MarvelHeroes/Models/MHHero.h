//
//  MHHeroe.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hero+CoreDataClass.h"

@interface MHHero : NSObject

@property(nonatomic, strong)NSString* heroID;

@property(nonatomic, strong)NSString* name;

@property(nonatomic, strong)NSString* heroDescription;

@property(nonatomic, strong)NSString* imageUrl;

@property(nonatomic, strong)NSData* image;

@property(nonatomic, assign)BOOL isFavorite;


- (instancetype)initWithJson:(NSDictionary*)json;

- (instancetype)initWithObj:(Hero*)heroObj;
    
@end
