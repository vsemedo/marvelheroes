//
//  MHDataManager.h
//  MarvelHeroes
//
//  Created by Victor Tavares on 21/05/17.
//  Copyright © 2017 MC1. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Hero+CoreDataClass.h"
#import "MHHero.h"

@interface MHDataManager : NSObject

+ (NSManagedObjectContext*)getContext;

+ (Hero*)saveHero:(MHHero*)hero;

+ (BOOL)deleteHero:(NSString*)heroID;

+ (Hero*)fetchHero:(NSString*)heroID;

+ (NSArray*)fetchHeroes;

@end
